package models;

public class CustomerCountry {
    private String country;
    private int numCustomers;

    public CustomerCountry(String country, int numCustomers) {
        this.country = country;
        this.numCustomers = numCustomers;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getNumCustomers() {
        return numCustomers;
    }

    public void setNumCustomers(int numCustomers) {
        this.numCustomers = numCustomers;
    }
}

