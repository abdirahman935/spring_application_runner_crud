package models;

public class CustomerSpender {
    private int customerId;
    private double totalSpent;

    public CustomerSpender(int customerId, double totalSpent) {
        this.customerId = customerId;
        this.totalSpent = totalSpent;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public double getTotalSpent() {
        return totalSpent;
    }

    public void setTotalSpent(double totalSpent) {
        this.totalSpent = totalSpent;
    }
}

