package models;


public class CustomerGenre {
    private int customerId;
    private String genre;

    public CustomerGenre(int customerId, String genre) {
        this.customerId = customerId;
        this.genre = genre;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}

