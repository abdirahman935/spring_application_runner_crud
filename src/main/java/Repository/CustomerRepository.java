package Repository;

import models.Customer;
import org.springframework.jdbc.core.JdbcTemplate;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerRepository {

    private Connection conn;
    public CustomerRepository() throws SQLException {
        String url = "jdbc:postgresql://localhost:5432/Album_database";
        String username = "postgresql";
        String password = "Cabey";
        this.conn = DriverManager.getConnection(url, username, password);
    }

//private JdbcTemplate jdbcTemplate = new JdbcTemplate();
//    private Connection conn;
//
//    public CustomerRepository() {
//        this.jdbcTemplate = jdbcTemplate;
//    }


    //This method retrieves all customers from the customer table and returns them as a list of Customer objects
    //This method throws a SQLException if there is an error executing the SQL statement
    public List<Customer> getAllCustomers() throws SQLException {
        List<Customer> customers = new ArrayList<>();

        try (Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM customers")) {
            while (rs.next()) {
                int id = rs.getInt("customerId");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String country = rs.getString("country");
                String postalCode = rs.getString("postalCode");
                String phoneNumber = rs.getString("phone");
                String email = rs.getString("email");

                customers.add(new Customer(id, firstName, lastName, country, postalCode, phoneNumber, email));
            }
        }

        return customers;
    }
  //This method retrieves a customer from the table by their ID and returns a Customer
    public Customer getCustomerById(int id) throws SQLException {
        Customer customer = null;

        String sql = "SELECT * FROM customers WHERE CustomerId = ?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, id);

            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    String firstName = rs.getString("firstName");
                    String lastName = rs.getString("lastName");
                    String country = rs.getString("country");
                    String postalCode = rs.getString("postalCode");
                    String phoneNumber = rs.getString("phone");
                    String email = rs.getString("email");

                    customer = new Customer(id, firstName, lastName, country, postalCode, phoneNumber, email);
                }
            }
        }

        return customer;
    }
   //This method retrieves all customers from the table whose first or last name matches the given string and returns them as a list of Customer
    public List<Customer> getCustomersByName(String name) throws SQLException {
        List<Customer> customers = new ArrayList<>();

        String sql = "SELECT * FROM customers WHERE first_name LIKE john OR last_name LIKE petterson";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, "%" + name + "%");
            stmt.setString(2, "%" + name + "%");

            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    int id = rs.getInt("customerId");
                    String firstName = rs.getString("firstName");
                    String lastName = rs.getString("lastName");
                    String country = rs.getString("country");
                    String postalCode = rs.getString("postalCode");
                    String phoneNumber = rs.getString("phone");
                    String email = rs.getString("email");

                    customers.add(new Customer(id, firstName, lastName, country, postalCode, phoneNumber, email));
                }
            }
        }

        return customers;
    }
  // This method retrieves a subset of customers from the table based on the given limit and offset and returns them as a list of Customer
    // and using the LIMIT and OFFSET term in SQL to limit the number of results returned
    public List<Customer> getCustomersByPage(int limit, int offset) throws SQLException {
        List<Customer> customers = new ArrayList<>();

        String sql = "SELECT * FROM customers ORDER BY CustomerId LIMIT ? OFFSET ?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setInt(1, limit);
            stmt.setInt(2, offset);

            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    int id = rs.getInt("CustomerId");
                    String firstName = rs.getString("FirstName");
                    String lastName = rs.getString("LastName");
                    String country = rs.getString("Country");
                    String postalCode = rs.getString("PostalCode");
                    String phoneNumber = rs.getString("Phone");
                    String email = rs.getString("Email");

                    customers.add(new Customer(id, firstName, lastName, country, postalCode, phoneNumber, email));
                }
            }
        }

        return customers;
    }
   //this method adds a new customer to the table and returns the ID of the newly inserted row
    public int addCustomer(Customer customer) throws SQLException {
        String query = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) VALUES (?, ?, ?, ?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(query))  {
            stmt.setString(1, customer.getFirstName());
            stmt.setString(2, customer.getLastName());
            stmt.setString(3, customer.getCountry());
            stmt.setString(4, customer.getPostalCode());
            stmt.setString(5, customer.getPhoneNumber());
            stmt.setString(6, customer.getEmail());
            stmt.executeUpdate();
        }
        return 0;
    }
   //This method updates an existing customer in the database with new data and returns nothing
    public void updateCustomer(Customer customer) throws SQLException {
        String query = "UPDATE customer SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? WHERE customer_id = ?";
        try (PreparedStatement stmt = conn.prepareStatement(query))  {
            stmt.setString(1, customer.getFirstName());
            stmt.setString(2, customer.getLastName());
            stmt.setString(3, customer.getCountry());
            stmt.setString(4, customer.getPostalCode());
            stmt.setString(5, customer.getPhoneNumber());
            stmt.setString(6, customer.getEmail());
            stmt.setInt(7, customer.getId());
            stmt.executeUpdate();
        }
    }
   // This method retrieves the country with the most customers and returns the country name as a String.
   //  used a SQL query with the GROUP BY and ORDER BY term to aggregate the customer data and find the country with the most customers.
    public String getCountryWithMostCustomers() throws SQLException {
        String query = "SELECT country, COUNT(*) AS customer_count FROM customer GROUP BY country ORDER BY customer_count DESC LIMIT 1";
        try (PreparedStatement stmt = conn.prepareStatement(query)) {
        ResultSet rs = stmt.executeQuery(query);
            if (rs.next()) {
                return rs.getString("country");
            }
            return null;
        }
    }
  // This method retrieves the customer who has spent the most money and returns a Customer object.
  // used a SQL query with the INNER JOIN, GROUP BY, and ORDER BY term to join the customer and invoice tables, aggregate the spending data, and
  // find the customer with the highest spending.
    public Customer getHighestSpender() throws SQLException {
        String query = "SELECT c.customer_id, c.first_name, c.last_name, c.country, c.postal_code, c.phone, c.email, SUM(i.total) AS total_spent FROM customer c INNER JOIN invoice i ON c.customer_id = i.customer_id GROUP BY c.customer_id ORDER BY total_spent DESC LIMIT 1";
        try (PreparedStatement stmt = conn.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery(query);
            if (rs.next()) {
                return extractCustomerFromResultSet(rs);
            }
            return null;
        }
    }

    //it is a private helper method that extracts a Customer object from a ResultSet.
    // It is used by getHighestSpender() to create a Customer object from the result set returned by the SQL query
    private Customer extractCustomerFromResultSet(ResultSet rs) throws SQLException {
        int id = rs.getInt("customer_id");
        String firstName = rs.getString("first_name");
        String lastName = rs.getString("last_name");
        String country = rs.getString("country");
        String postalCode = rs.getString("postal_code");
        String phoneNumber = rs.getString("phone");
        String email = rs.getString("email");
        return new Customer(id, firstName, lastName, country, postalCode, phoneNumber, email);
    }

    public String getMostPopularGenre(int customerId) throws SQLException {
        String query = "SELECT g.name, COUNT(*) AS track_count FROM customer c INNER JOIN invoice i ON c.customer_id = i.customer_id INNER JOIN invoice_line il ON i.invoice_id = il.invoice_id INNER JOIN track t ON il.track_id = t.track_id INNER JOIN genre g ON t.genre_id = g.genre_id WHERE c.customer_id = ? GROUP BY g.name ORDER BY track_count DESC LIMIT 1";
        try (PreparedStatement stmt = conn.prepareStatement(query))  {
            stmt.setInt(1, customerId);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    return rs.getString("name");
                }
                return null;
            }
        }
    }
}