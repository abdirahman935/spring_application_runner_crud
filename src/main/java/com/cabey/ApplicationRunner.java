package com.cabey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.SQLException;
import java.util.List;
import models.Customer;
import models.CustomerCountry;
import models.CustomerGenre;
import models.CustomerSpender;
import Repository.CustomerRepository;


@SpringBootApplication
public class ApplicationRunner {
            public static void main(String[] args) throws SQLException {
                SpringApplication.run(ApplicationRunner.class, args);
                // Create a new instance of the CustomerRepository
                CustomerRepository customerRepository = new CustomerRepository();

                try {
                    // Read all customers from the database and print them to the console
                    System.out.println("All customers:");
                    List<Customer> allCustomers = customerRepository.getAllCustomers();
                    for (Customer customer : allCustomers) {
                        System.out.println(customer);
                    }
                    System.out.println();

                    // Read a specific customer by ID and print the result to the console
                    System.out.println("Customer with ID 54:");
                    Customer customerById = customerRepository.getCustomerById(1);
                    System.out.println(customerById);
                    System.out.println();

                    // Read a specific customer by name and print the result to the console
                    System.out.println("Customer with name 'John Doe':");
                    Customer customerByName = (Customer) customerRepository.getCustomersByName("John");
                    System.out.println(customerByName);
                    System.out.println();

                    // Get a page of customers and print the results to the console
                    System.out.println("Page 2 of customers (3 per page):");
                    List<Customer> pageOfCustomers = customerRepository.getCustomersByPage(3, 3);
                    for (Customer customer : pageOfCustomers) {
                        System.out.println(customer);
                    }
                    System.out.println();

                    // Add a new customer to the database and print the result to the console
                    System.out.println("Adding a new customer:");
                    Customer newCustomer = new Customer(744, "Doe", "jonas", "Norway", "12345", "123-456-7890", "jonas@example.com");
                    int newCustomerId = customerRepository.addCustomer(newCustomer);
                    System.out.println("New customer ID: " + newCustomerId);
                    System.out.println(customerRepository.getCustomerById(newCustomerId));
                    System.out.println();

                    // Update an existing customer in the database and print the result to the console
                    System.out.println("Updating an existing customer:");
                    Customer updatedCustomer = new Customer(customerById.getId(), "John", "Smith", "USA", "12345", "123-456-7890", "john.smith@example.com");
                    customerRepository.updateCustomer(updatedCustomer);
                    System.out.println(customerRepository.getCustomerById(customerById.getId()));
                    System.out.println();

                    // Get the country with the most customers and print the result to the console
                    System.out.println("Country with the most customers:");
                    String customerCountry = customerRepository.getCountryWithMostCustomers();
                    System.out.println(customerCountry);
                    System.out.println();

                    // Get the customer who has spent the most money and print the result to the console
                    System.out.println("Customer who has spent the most:");
                    Customer customerSpender = customerRepository.getHighestSpender();
                    System.out.println(customerSpender);
                    System.out.println();

                    // Get the most popular genre for a given customer and print the result to the console
                    System.out.println("Most popular genre for customer with ID 1:");
                    String customerGenre = customerRepository.getMostPopularGenre(1);
                    System.out.println(customerGenre);
                    System.out.println();

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

